import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:privatewebsite/pages/home/backend/model/structs.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends ResponsivePage {
  @override
  Widget buildBody(BuildContext context, SizingInformation sizingInformation, ResponsiveState state) {
    final theme = Theme.of(context);
    return FutureBuilder<HomePageContent>(
        future: getContent(context),
        builder: (context, snapshot) {
          final data = snapshot.data;
          if (data == null) {
            return const SizedBox(
              height: 60,
              width: 60,
              child: CircularProgressIndicator(),
            );
          }
          return Column(
            children: [
              Row(
                mainAxisAlignment: state == ResponsiveState.small ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.spaceBetween,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      AnimatedSize(
                        duration: const Duration(milliseconds: 10),
                        child: SvgPicture.asset(
                          HomePageContent.backgroundImagePath,
                          width: sizingInformation.localWidgetSize.width * (state == ResponsiveState.small ? 0.4 : 0.3),
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: sizingInformation.localWidgetSize.width * (state == ResponsiveState.small ? 0.06 : 0.04)),
                        child: ClipOval(
                          child: Image.asset(
                            HomePageContent.imagePath,
                            width: sizingInformation.localWidgetSize.width * (state == ResponsiveState.small ? 0.2 : 0.15),
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (state != ResponsiveState.small) buildGreeting(sizingInformation, state, data, theme)
                ],
              ),
              if (state == ResponsiveState.small)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 42),
                  child: buildGreeting(sizingInformation, state, data, theme),
                ),
              if (state != ResponsiveState.small) const SizedBox(height: 100),
              Text(
                data.shortText,
                style: theme.textTheme.bodyLarge?.copyWith(
                  fontSize: FontSizeMaker.getFontSize(
                    sizingInformation.localWidgetSize.width,
                    ResponsiveFontSize.bodyLarge,
                  ),
                ),
                textAlign: TextAlign.center,
              ),
              // Text("TikTakToe")
            ],
          );
        });
  }

  Widget buildGreeting(SizingInformation sizingInformation, ResponsiveState state, HomePageContent data, ThemeData theme) {
    final greeting = AutoSizeText(
      data.greeting,
      style: theme.textTheme.bodyLarge?.copyWith(
        fontSize: FontSizeMaker.getFontSize(
          sizingInformation.localWidgetSize.width,
          ResponsiveFontSize.bodyLarge,
        ),
      ),
      textAlign: TextAlign.center,
      softWrap: true,
      maxLines: 3,
    );

    final nameIntroduction = Text(
      data.nameIntroduction,
      style: theme.textTheme.bodyLarge?.copyWith(
        fontSize: FontSizeMaker.getFontSize(
          sizingInformation.localWidgetSize.width,
          ResponsiveFontSize.bodyLarge,
        ),
      ),
      textAlign: TextAlign.center,
    );

    if (state != ResponsiveState.small) {
      return SizedBox(
        width: sizingInformation.localWidgetSize.width * 0.2,

        child: Column(
          children: [
            greeting,
            const SizedBox(height: 40),
            nameIntroduction,
          ],
        ),
      );
    } else {
      return Column(
        children: [
          greeting,
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: nameIntroduction,
          ),
        ],
      );
    }
  }

  Future<HomePageContent> getContent(BuildContext context) async {
    return Future.value(
      HomePageContent(
        "Herzlich willkommen in meiner kleinen Ecke des Internets!",
        "Ich bin Linus :)",
        "Hier erzähle ich der Welt ein wenig über mich oder spiele mit neuen Technologien rum und habe einfach meinen Spaß.",
      ),
    );
  }
}
