import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:privatewebsite/future_libs/descriptum_patronum/widgets/bullet_point_list.dart';
import 'package:privatewebsite/future_libs/descriptum_patronum/widgets/described_thing.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';

class EventContent extends StatelessWidget {
  const EventContent({
    super.key,
    required this.event,
    required this.state,
  });

  final ResponsiveState state;
  final LifeEvent event;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final localizations = strings(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 15),
      child: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),

        children: [
          Text(
            event.title,
            style: theme.textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold, color: theme.colorScheme.onSurfaceVariant),
          ),
          Text(
            "${DateFormat.yMMM().format(event.timeOfWork.start)} - ${event.timeOfWork.end != null ? DateFormat.yMMM().format(event.timeOfWork.end) : localizations.today}",
            style: theme.textTheme.labelMedium?.copyWith(fontStyle: FontStyle.italic),
          ),
          if (event.publicEntity != null)
            Text(
              event.publicEntity!.entityName,
              style: theme.textTheme.labelMedium?.copyWith(fontWeight: FontWeight.bold),
            ),
          if (event.description != null)
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                event.description!.text,
                style: theme.textTheme.labelMedium,
              ),
            ),
          BulletPointList(
            bulletPoints: event.bulletPoints,
            container: BulletContainer.container,
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: event.additionalInfo.length,
            itemBuilder: (context, index) {
              final info = event.additionalInfo[index];
              return DescribedThingWidget(
                description: info,
                state: state,
              );
            },
          ),
        ],
      ),
    );
  }
}
