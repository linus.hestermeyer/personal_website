import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/pages/career/ui/event_content.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:responsive_builder/src/sizing_information.dart';

class EventItem extends HookWidget {
  final int index;
  final double pixelPerDay;
  final LifeEvent event;
  final ValueNotifier<int?> hovered;
  final SizingInformation sizingInformation;
  final ResponsiveState state;

  const EventItem(this.index, this.pixelPerDay, this.event, this.hovered, this.sizingInformation, this.state, {super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final localizations = strings(context);
    return Container(
      height: hovered.value == index ? 300 : 42,
      width: pixelPerDay * event.timeOfWork.totalDays(),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(24)),
        boxShadow: [
          BoxShadow(
            color: theme.brightness == Brightness.light ? Colors.grey.withOpacity(0.7) : Colors.white.withOpacity(0.3),
            blurRadius: 7,
            offset: Offset(4, 4), // changes position of shadow
          ),
        ],
      ),
      child: InkWell(
        hoverColor: Colors.white.withOpacity(0),
        onTap: () {
          hovered.value = index;
          // showDialog(
          //   context: context,
          //   builder: (context) {
          //     return Dialog(
          //       insetPadding: EdgeInsets.all(10),
          //       child: Container(
          //         width: sizingInformation.localWidgetSize.width * 0.6,
          //         height: sizingInformation.localWidgetSize.height * 0.8,
          //         decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.lightBlue),
          //         padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
          //         child: Text("You can make cool stuff!", style: TextStyle(fontSize: 24), textAlign: TextAlign.center),
          //       ),
          //     );
          //   },
          // );
        },
        onHover: (event) async {
          if (event) {
            hovered.value = index;
          } else {
            await Future.delayed(const Duration(milliseconds: 1000), () {
              if (hovered.value == index) hovered.value = null;
            });
          }
        },
        child: Container(
          decoration: ShapeDecoration(
            color: event.category.colorForCategory(opacity: 0.3),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
          ),
          padding: const EdgeInsets.all(8.0),
          child: hovered.value != index
              ? Center(
                  child: RotatedBox(
                    quarterTurns: 0,
                    child: AutoSizeText(
                      event.title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: theme.textTheme.labelLarge?.copyWith(color: theme.colorScheme.onSurfaceVariant),
                    ),
                  ),
                )
              : EventContent(event: event, state:state),
        ),
      ),
    );
  }
}
