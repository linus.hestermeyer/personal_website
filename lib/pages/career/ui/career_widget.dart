import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/pages/career/ui/chart_widget.dart';
import 'package:privatewebsite/pages/career/ui/list_widget.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:privatewebsite/shared/ui/widgets/my_chip.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CareerWidget extends HookWidget {
  final SizingInformation sizingInformation;
  final ResponsiveState state;

  const CareerWidget(this.sizingInformation, this.state, {super.key});

  @override
  Widget build(BuildContext context) {
    final localizations = strings(context);
    final filter = useState<Category?>(null);
    final listMode = useState(false);
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Text(localizations.runway, style: theme.textTheme.bodyLarge),
        ),
        buildFilterHeader(filter, localizations, listMode),
        if (!listMode.value) ChartWidget(filter.value),
        if (listMode.value) ListWidget(state),
      ],
    );
  }

  Widget buildFilterHeader(ValueNotifier<Category?> filter, AppLocalizations localizations, ValueNotifier<bool> listMode) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (!listMode.value)
            SimpleChip(
              label: "Als Liste ansehen",
              icon: CupertinoIcons.square_list,
              onTap: () {
                listMode.value = true;
              },
            ),
          if (listMode.value)
            SimpleChip(
              label: "Als Diagramm",
              icon: CupertinoIcons.chart_bar_square,
              onTap: () {
                listMode.value = false;
              },
            ),
          if (state == ResponsiveState.small && filter.value != null && !listMode.value)
            SimpleChip(
              icon: Icons.filter_list_off,
              onTap: () {
                filter.value = null;
              },
            ),
          if (state == ResponsiveState.small && filter.value == null && !listMode.value)
            PopupMenuButton<Category?>(
              child: SimpleChip(icon: Icons.filter_list_alt),
              initialValue: null,
              // Callback that sets the selected popup menu item.

              onSelected: (Category? category) {
                if (filter.value == category) {
                  filter.value = null;
                } else {
                  filter.value = category;
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<Category>>[
                PopupMenuItem<Category>(value: Category.projects, child: Text(Category.projects.textForCategory(localizations))),
                PopupMenuItem<Category>(value: Category.career, child: Text(Category.career.textForCategory(localizations))),
                PopupMenuItem<Category>(value: Category.education, child: Text(Category.education.textForCategory(localizations))),
                PopupMenuItem<Category>(value: Category.social, child: Text(Category.social.textForCategory(localizations))),
              ],
            ),
          if (state != ResponsiveState.small && !listMode.value)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                wrapFilter(Category.projects, localizations, filter),
                wrapFilter(Category.career, localizations, filter),
                wrapFilter(Category.education, localizations, filter),
                wrapFilter(Category.social, localizations, filter),
              ],
            )
        ],
      ),
    );
  }

  Widget wrapFilter(Category category, AppLocalizations localizations, ValueNotifier<Category?> filter) {
    return SimpleChip(
      label: category.textForCategory(localizations),
      tint: category.colorForCategory(),
      selected: filter.value == category,
      onTap: () {
        print("OnTap: ${filter.value}");
        if (filter.value == category) {
          filter.value = null;
        } else {
          filter.value = category;
        }
      },
    );
  }
}
