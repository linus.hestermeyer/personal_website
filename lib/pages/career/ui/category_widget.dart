import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:intl/intl.dart';
import 'package:privatewebsite/future_libs/descriptum_patronum/widgets/bullet_point_list.dart';
import 'package:privatewebsite/future_libs/descriptum_patronum/widgets/described_thing.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';

class CategoryWidget extends HookWidget {
  final Category category;
  final List<LifeEvent> events;
  final ResponsiveState state;

  const CategoryWidget({super.key, required this.category, required this.events, required this.state});

  @override
  Widget build(BuildContext context) {
    final localizations = strings(context);
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          category.textForCategory(localizations),
          style: theme.textTheme.headlineSmall,
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: events.length,
          itemBuilder: (context, index) {
            final event = events[index];
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 4,
                    child: Text(
                      "${DateFormat.yMMM().format(event.timeOfWork.start)} - ${dateFits(event) ? DateFormat.yMMM().format(event.timeOfWork.end) : localizations.today}",
                      style: theme.textTheme.bodyMedium,
                    ),
                  ),
                  buildInfo(event, theme),
                ],
              ),
            );
          },
        ),
      ],
    );
  }

  Expanded buildInfo(LifeEvent event, ThemeData theme) {
    return Expanded(
      flex: 6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (event.publicEntity != null)
            Text(
              event.publicEntity!.entityName,
              style: theme.textTheme.bodyMedium,
            ),
          Text(
            event.title,
            style: theme.textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
          ),
          if (event.description != null)
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                event.description!.text,
                style: theme.textTheme.bodyMedium,
              ),
            ),
          BulletPointList(
            bulletPoints: event.bulletPoints,
            container: BulletContainer.surface,
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: event.additionalInfo.length,
            itemBuilder: (context, index) {
              final info = event.additionalInfo[index];
              return DescribedThingWidget(
                description: info,
                state: state,
              );
            },
          ),
        ],
      ),
    );
  }

  bool dateFits(LifeEvent event) {
    final day = event.timeOfWork.end.day == DateTime.now().day;
    final month = event.timeOfWork.end.month == DateTime.now().month;
    final year = event.timeOfWork.end.year == DateTime.now().year;
    return day && month && year;
  }
}
