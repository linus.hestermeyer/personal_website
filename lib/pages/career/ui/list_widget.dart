import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/pages/career/backend/port.dart';
import 'package:privatewebsite/pages/career/ui/category_widget.dart';
import 'package:privatewebsite/pages/career/ui/event_content.dart';
import 'package:privatewebsite/pages/career/ui/event_item.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ListWidget extends HookWidget {
  final ResponsiveState state;
  const ListWidget( this. state, {super.key});

  @override
  Widget build(BuildContext context) {
    final hovered = useState<int?>(null);
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        final width = sizingInformation.localWidgetSize.width;
        final height = width / 1.8;
        return FutureBuilder<List<LifeEvent>>(
            future: PrimPortImpl().getAllEvents(),
            builder: (context, snapshot) {
              if (snapshot.data == null) return const CircularProgressIndicator();
              var events = snapshot.data!;

              final firstDate = getFirstDate(events);
              final lastDate = DateTime.now();

              final days = lastDate.difference(firstDate).inDays;
              final pixelPerDay = width / days;

              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),

                itemCount: Category.values.length,
                itemBuilder: (context, index) {
                  final category = Category.values[index];

                  return CategoryWidget(category: category, events: events.where((element) => element.category == category).toList(), state:state);
                },
              );
            });
      },
    );
  }

  DateTime getFirstDate(List<LifeEvent> events) {
    DateTime firstDate = events[0].timeOfWork.start;
    for (var event in events) {
      if (event.timeOfWork.start.isBefore(firstDate)) {
        firstDate = event.timeOfWork.start;
      }
    }

    return firstDate;
  }
}
