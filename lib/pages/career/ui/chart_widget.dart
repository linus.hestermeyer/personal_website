import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/pages/career/backend/model/structs.dart';
import 'package:privatewebsite/pages/career/backend/port.dart';
import 'package:privatewebsite/pages/career/ui/event_item.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChartWidget extends HookWidget {
  static const _magicValue = 250;
  final Category? filter;

  const ChartWidget(this.filter, {super.key});

  @override
  Widget build(BuildContext context) {
    final hovered = useState<int?>(null);
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        final width = sizingInformation.localWidgetSize.width;
        final height = width / 1.8;
        return FutureBuilder<List<LifeEvent>>(
          future: PrimPortImpl().getAllEvents(),
          builder: (context, snapshot) {
            if (snapshot.data == null) return const CircularProgressIndicator();
            var events = snapshot.data!;

            if (filter != null) {
              events = events.where((element) => element.category == filter).toList();
            }

            final firstDate = getFirstDate(events);
            final lastDate = DateTime.now();

            final days = lastDate.difference(firstDate).inDays;
            final pixelPerDay = width / days;

            final size = sizingInformation.screenSize;

            late ResponsiveState state;
            if (size.width < ResponsivePage.maxWidthSmall) state = ResponsiveState.small;
            if (size.width >= ResponsivePage.maxWidthSmall && size.width < ResponsivePage.maxWidthMedium) state = ResponsiveState.medium;
            if (size.width >= ResponsivePage.maxWidthMedium) state = ResponsiveState.large;

            return SizedBox(
              width: pixelPerDay * days,
              height: (events.length * 52) + 100 + (hovered.value != null ? _magicValue : 0).toDouble(),
              child: Stack(
                children: getChildren(events, pixelPerDay, days, firstDate, lastDate, sizingInformation, hovered, state),
              ),
            );
          },
        );
      },
    );
  }

  DateTime getFirstDate(List<LifeEvent> events) {
    DateTime firstDate = events[0].timeOfWork.start;
    for (var event in events) {
      if (event.timeOfWork.start.isBefore(firstDate)) {
        firstDate = event.timeOfWork.start;
      }
    }

    return firstDate;
  }

  List<Widget> getChildren(
    List<LifeEvent> events,
    double pixelPerDay,
    int days,
    DateTime firstDate,
    DateTime lastDate,
    SizingInformation sizingInformation,
    ValueNotifier<int?> hovered,
    ResponsiveState state,
  ) {
    final result = List<Widget>.empty(growable: true);
    result.addAll(_linesAndYears(events, pixelPerDay, days, firstDate, lastDate, sizingInformation, hovered));

    for (var event in events) {
      int index = events.indexOf(event);
      double magischerWertVonHovered = getExtraPaddinGForHoveredState(hovered, index);

      result.add(
        Positioned(
          top: (index * 52) + 70 + magischerWertVonHovered,
          left: pixelPerDay * event.timeOfWork.start.difference(firstDate).inDays,
          child: EventItem(index, pixelPerDay, event, hovered, sizingInformation, state),
        ),
      );
    }

    return result;
  }

  double getExtraPaddinGForHoveredState(ValueNotifier<int?> hovered, int index) {
    double magischerWertVonHovered = 0;
    if (hovered.value != null && index > hovered.value!) {
      magischerWertVonHovered += _magicValue;
    }
    return magischerWertVonHovered;
  }

  List<Widget> _linesAndYears(
    List<LifeEvent> events,
    double pixelPerDay,
    int days,
    DateTime firstDate,
    DateTime lastDate,
    SizingInformation sizingInformation,
    ValueNotifier<int?> hovered,
  ) {
    final result = List<Widget>.empty(growable: true);

    final daysFromStartToNextYear = DateTime(firstDate.year + 1, 1, 1).difference(firstDate).inDays;
    double dayForYearLine = daysFromStartToNextYear.toDouble();
    DateTime indexDate = DateTime(firstDate.year + 1, 1, 1);
    while (indexDate.isBefore(lastDate)) {
      result.add(
        Positioned(
          left: pixelPerDay * dayForYearLine,
          child: Column(
            children: [
              Text("${indexDate.year}"),
              const SizedBox(height: 25),
              Container(
                color: Colors.grey,
                width: 1,
                height: ((events.length * 52) + (hovered.value != null ? _magicValue : 0)).toDouble(),
              ),
            ],
          ),
        ),
      );
      indexDate = DateTime(indexDate.year + 1, 1, 1);
      dayForYearLine += 365.25;
    }
    //todo: will have prpoblem on 1. january each year
    result.add(
      Positioned(
        left: pixelPerDay * lastDate.difference(firstDate).inDays.toDouble(),
        child: Column(
          children: [
            Text("TD"),
            const SizedBox(height: 25),
            Container(
              color: Colors.grey,
              width: 1,
              height: 500,
            ),
          ],
        ),
      ),
    );
    return result;
  }
}
