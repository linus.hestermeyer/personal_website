import 'package:flutter/material.dart';
import 'package:privatewebsite/future_libs/descriptum_patronum/model/structs.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';

class DescribedThingWidget extends StatelessWidget {
  final DescribedThing description;
  final ResponsiveState state;

  const DescribedThingWidget({super.key, required this.description, required this.state});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final header = SizedBox(
      width: 50,
      child: Text(
        description.title ?? "",
        style: theme.textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold),
      ),
    );
    final content = ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: description.descriptions.length,
      itemBuilder: (context, index) {
        final thing = description.descriptions[index];
        if (thing is AdditionalInfo) {
          final info = thing as AdditionalInfo;
          return Text(info.info, style: theme.textTheme.labelLarge);
        }
        return const SizedBox();
      },
    );

    if (state == ResponsiveState.small) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [header, const SizedBox(height: 5), content],
      );
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [header, const SizedBox(width: 30), content],
    );
  }
}
