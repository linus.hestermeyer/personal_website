// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    HomePageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: HomePage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    ProjectsPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const ProjectsPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    ProjectDetailPageRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<ProjectDetailPageRouteArgs>(
          orElse: () => ProjectDetailPageRouteArgs(
              projectId: pathParams.getString('projectId')));
      return CustomPage<dynamic>(
        routeData: routeData,
        child: ProjectDetailPage(
          args.projectId,
          key: args.key,
        ),
        opaque: true,
        barrierDismissible: false,
      );
    },
    CareerPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const CareerPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    InterestPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: InterestPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    ManifestPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: ManifestPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    ImpressumPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: ImpressumPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
    DataPrivacyPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: DataPrivacyPage(),
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(
          HomePageRoute.name,
          path: '/',
        ),
        RouteConfig(
          ProjectsPageRoute.name,
          path: '/projects',
        ),
        RouteConfig(
          ProjectDetailPageRoute.name,
          path: '/projects/:projectId',
        ),
        RouteConfig(
          CareerPageRoute.name,
          path: '/career-page',
        ),
        RouteConfig(
          InterestPageRoute.name,
          path: '/interest-page',
        ),
        RouteConfig(
          ManifestPageRoute.name,
          path: '/manifest-page',
        ),
        RouteConfig(
          ImpressumPageRoute.name,
          path: '/impressum-page',
        ),
        RouteConfig(
          DataPrivacyPageRoute.name,
          path: '/data-privacy-page',
        ),
      ];
}

/// generated route for
/// [HomePage]
class HomePageRoute extends PageRouteInfo<void> {
  const HomePageRoute()
      : super(
          HomePageRoute.name,
          path: '/',
        );

  static const String name = 'HomePageRoute';
}

/// generated route for
/// [ProjectsPage]
class ProjectsPageRoute extends PageRouteInfo<void> {
  const ProjectsPageRoute()
      : super(
          ProjectsPageRoute.name,
          path: '/projects',
        );

  static const String name = 'ProjectsPageRoute';
}

/// generated route for
/// [ProjectDetailPage]
class ProjectDetailPageRoute extends PageRouteInfo<ProjectDetailPageRouteArgs> {
  ProjectDetailPageRoute({
    required String projectId,
    Key? key,
  }) : super(
          ProjectDetailPageRoute.name,
          path: '/projects/:projectId',
          args: ProjectDetailPageRouteArgs(
            projectId: projectId,
            key: key,
          ),
          rawPathParams: {'projectId': projectId},
        );

  static const String name = 'ProjectDetailPageRoute';
}

class ProjectDetailPageRouteArgs {
  const ProjectDetailPageRouteArgs({
    required this.projectId,
    this.key,
  });

  final String projectId;

  final Key? key;

  @override
  String toString() {
    return 'ProjectDetailPageRouteArgs{projectId: $projectId, key: $key}';
  }
}

/// generated route for
/// [CareerPage]
class CareerPageRoute extends PageRouteInfo<void> {
  const CareerPageRoute()
      : super(
          CareerPageRoute.name,
          path: '/career-page',
        );

  static const String name = 'CareerPageRoute';
}

/// generated route for
/// [InterestPage]
class InterestPageRoute extends PageRouteInfo<void> {
  const InterestPageRoute()
      : super(
          InterestPageRoute.name,
          path: '/interest-page',
        );

  static const String name = 'InterestPageRoute';
}

/// generated route for
/// [ManifestPage]
class ManifestPageRoute extends PageRouteInfo<void> {
  const ManifestPageRoute()
      : super(
          ManifestPageRoute.name,
          path: '/manifest-page',
        );

  static const String name = 'ManifestPageRoute';
}

/// generated route for
/// [ImpressumPage]
class ImpressumPageRoute extends PageRouteInfo<void> {
  const ImpressumPageRoute()
      : super(
          ImpressumPageRoute.name,
          path: '/impressum-page',
        );

  static const String name = 'ImpressumPageRoute';
}

/// generated route for
/// [DataPrivacyPage]
class DataPrivacyPageRoute extends PageRouteInfo<void> {
  const DataPrivacyPageRoute()
      : super(
          DataPrivacyPageRoute.name,
          path: '/data-privacy-page',
        );

  static const String name = 'DataPrivacyPageRoute';
}
