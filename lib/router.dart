import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';
import 'package:injectable/injectable.dart';
import 'package:privatewebsite/pages/career/ui/page.dart';
import 'package:privatewebsite/pages/home/ui/page.dart';
import 'package:privatewebsite/pages/interests/ui/page.dart';
import 'package:privatewebsite/pages/legal/ui/impressum.dart';
import 'package:privatewebsite/pages/legal/ui/privacy.dart';
import 'package:privatewebsite/pages/manifest/ui/page.dart';
import 'package:privatewebsite/pages/projects/ui/detail_page.dart';
import 'package:privatewebsite/pages/projects/ui/list_page.dart';

part 'router.gr.dart';

//Todo: refector names - currently they are the same as in the PageEnum, but one cant see that its a route while reading the code

@lazySingleton
@CustomAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: ProjectsPage, path: '/projects'),
    AutoRoute(page: ProjectDetailPage, path: '/projects/:projectId'),
    AutoRoute(page: CareerPage),
    AutoRoute(page: InterestPage),
    AutoRoute(page: ManifestPage),
    AutoRoute(page: ImpressumPage),
    AutoRoute(page: DataPrivacyPage),
    // AutoRoute(page: ManifestPage, name: "Manifest"),
    // AutoRoute(page: ManifestPage, name: "Manifest"),
  ],
)
class AppRouter extends _$AppRouter {
  AppRouter() : super();
}
