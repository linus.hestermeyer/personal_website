import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';

class SimpleChip extends HookWidget {
  final String? label;
  final IconData? icon;
  final Color? tint;
  final bool selected;
  final Function()? onTap;

  const SimpleChip({this.label, this.icon, this.tint, this.selected = false, super.key, this.onTap});

  static Color _defautlTint = Color.fromRGBO(98, 98, 98, 1);

  @override
  Widget build(BuildContext context) {
    final loc = strings(context);
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 36,
          padding: const EdgeInsets.all(8),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: selected ? const Color.fromRGBO(136, 136, 136, 1) : const Color.fromRGBO(217, 217, 217, 1),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(
                color: theme.brightness == Brightness.light ? Colors.grey.withOpacity(0.3) : Colors.white.withOpacity(0.3),
                blurRadius: 7,
                offset: const Offset(4, 4), // changes position of shadow
              ),
            ],
          ),
          child: Row(
            children: [
              if (icon != null)
                Padding(
                  padding: EdgeInsets.only(right: label != null ? 5.0 : 0),
                  child: Icon(
                    icon,
                    color: tint ?? _defautlTint,
                    size: 20,
                  ),
                ),
              if (label != null)
                Text(
                  label!,
                  style: theme.textTheme.labelMedium?.copyWith(color: (tint ?? _defautlTint)),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
