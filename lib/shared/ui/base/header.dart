import 'package:auto_route/auto_route.dart';
import 'package:auto_route/src/route/page_route_info.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/router.dart';
import 'package:privatewebsite/service_locator.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:privatewebsite/shared/ui/base/page.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Header extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ResponsiveBuilder(
      builder: (BuildContext context, SizingInformation sizingInformation) {
        final size = sizingInformation.screenSize;

        late ResponsiveState state;
        if (size.width < ResponsivePage.maxWidthSmall) state = ResponsiveState.small;
        if (size.width >= ResponsivePage.maxWidthSmall && size.width < ResponsivePage.maxWidthMedium) state = ResponsiveState.medium;
        if (size.width >= ResponsivePage.maxWidthMedium) state = ResponsiveState.large;

        return Center(
          child: SizedBox(
            width: size.width * 0.8,
            height: state == ResponsiveState.large ? 100 : 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (state == ResponsiveState.small)
                  InkWell(
                    child: Icon(Icons.menu),
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                if (state == ResponsiveState.small)
                  Text(
                    WebsitePage.currentRoute().getTitle(context),
                    style: TextStyle(
                      fontSize: state == ResponsiveState.large ? 28 : 18,
                      fontWeight: FontWeight.w300,
                      color: theme.colorScheme.onSurface,
                    ),
                  ),
                Center(
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(state == ResponsiveState.large ? 10 : 7),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(
                            side: BorderSide(
                              width: 1,
                              color: theme.colorScheme.onSurface,
                            ),
                          ),
                        ),
                        child: Text(
                          "Lh",
                          style: TextStyle(color: theme.colorScheme.onSurface, fontSize: state == ResponsiveState.large ? 28 : 20),
                        ),
                      ),
                    ],
                  ),
                ),
                if (state != ResponsiveState.small)
                  ListView(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    children: List.generate(
                      WebsitePage.headerPages().length,
                      (index) {
                        return AppBarLink(page: WebsitePage.headerPages()[index], state: state);
                      },
                    ),
                  )
              ],
            ),
          ),
        );
      },
    );
  }
}

enum WebsitePage {
  //dont change these to lowercase yet, since i still reruie "nam" in the navigation
  homePage,
  projectsPage,
  projectDetailPage,
  careerPage,
  interestPage,
  manifestPage,
  impressumPage,
  dataPrivacyPage;

  static List<WebsitePage> headerPages() => [
        homePage,
        projectsPage,
        careerPage,
      ]; // interestPage, manifestPage

  String getTitle(BuildContext context) {
    final localizations = strings(context);
    switch (this) {
      case WebsitePage.homePage:
        return localizations.page_home_title;
      case WebsitePage.projectsPage:
        return localizations.page_projects_title;
      case WebsitePage.projectDetailPage:
        return localizations.page_projects_title;
      case WebsitePage.careerPage:
        return localizations.page_career_title;
      case WebsitePage.interestPage:
        return localizations.page_interests_title;
      case WebsitePage.manifestPage:
        return localizations.page_manifest_title;
      case WebsitePage.impressumPage:
        return localizations.impressum;
      case WebsitePage.dataPrivacyPage:
        return localizations.datenschutz;
    }
  }

  static WebsitePage currentRoute() {
    final router = locator<AppRouter>();
    final current = router.current.name.toLowerCase();
    for (var element in WebsitePage.values) {
      if (("${element.name}Route").toLowerCase() == current) {
        return element;
      }
    }
    print("Non found: $current");
    return WebsitePage.homePage;
  }

  PageRouteInfo routeForPage() {
    switch (this) {
      case WebsitePage.homePage:
        return const HomePageRoute();
      case WebsitePage.projectDetailPage:
      case WebsitePage.projectsPage:
        return const ProjectsPageRoute();
      case WebsitePage.careerPage:
        return const CareerPageRoute();
      case WebsitePage.interestPage:
        return const InterestPageRoute();
      case WebsitePage.manifestPage:
        return const ManifestPageRoute();
      case WebsitePage.impressumPage:
        return const ImpressumPageRoute();
      case WebsitePage.dataPrivacyPage:
        return const DataPrivacyPageRoute();
    }
  }
}

class AppBarLink extends HookWidget {
  final WebsitePage page;
  final ResponsiveState state;

  const AppBarLink({super.key, required this.page, required this.state});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final hovered = useState(false);

    final router = locator<AppRouter>();
    bool selected = WebsitePage.currentRoute() == page;
    double horizontalMargin = state == ResponsiveState.large ? 15 : 5;
    return InkWell(
      onHover: (value) {
        hovered.value = value;
      },
      onTap: () {
        router.push(page.routeForPage());
      },
      child: Container(
        margin: EdgeInsets.only(left: horizontalMargin, right: horizontalMargin, top: 10),
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Text(
              page.getTitle(context),
              style: TextStyle(
                fontSize: state == ResponsiveState.large ? 28 : 18,
                fontWeight: (hovered.value || selected) ? FontWeight.bold : FontWeight.w300,
                color: selected ? theme.colorScheme.onSurface : const Color.fromRGBO(136, 136, 136, 1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
