import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:privatewebsite/router.dart';
import 'package:privatewebsite/service_locator.dart';
import 'package:privatewebsite/shared/ui/base/footer.dart';
import 'package:privatewebsite/shared/ui/base/header.dart';
import 'package:responsive_builder/responsive_builder.dart';
export 'package:privatewebsite/shared/util/font_size_maker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
export 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum ResponsiveState { small, medium, large }

AppLocalizations strings(BuildContext context) => AppLocalizations.of(context)!;

abstract class ResponsivePage extends HookWidget {
  static int largePage = 1792;
  static int maxWidthMedium = 1100;
  static int maxWidthSmall = 700;


  //Todo: so umbauen, dass ich diese werte überall mit nem ChangeNotifierConsumer nutzen kann
  const ResponsivePage({super.key});

  @override
  Widget build(BuildContext context) {
    final localizations = strings(context);
    final theme = Theme.of(context);
    return Scaffold(
      drawer: Drawer(
        width: 150,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                child: Text(
                  localizations.drawer_header,
                  style: theme.textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
                ),
                margin: null,
                padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
              ),
              buildNavItem(context, WebsitePage.homePage),
              buildNavItem(context, WebsitePage.projectsPage),
              buildNavItem(context, WebsitePage.careerPage),
              // buildNavItem(context, WebsitePage.interestPage),
              // buildNavItem(context, WebsitePage.manifestPage),
              Padding(padding: const EdgeInsets.symmetric(horizontal: 5)),
              buildNavItem(context, WebsitePage.impressumPage),
              buildNavItem(context, WebsitePage.dataPrivacyPage),
            ],
          ),
        ),
      ),
      body: ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
        final size = sizingInformation.screenSize;

        late ResponsiveState state;
        if (size.width < maxWidthSmall) state = ResponsiveState.small;
        if (size.width >= maxWidthSmall && size.width < maxWidthMedium) state = ResponsiveState.medium;
        if (size.width >= maxWidthMedium) state = ResponsiveState.large;

        final horizontal = paddingForState(sizingInformation, state);
        final vertical = verticalPaddingForState(sizingInformation, state);
        return CustomScrollView(
          primary: true,
          slivers: [
            //todo: Drawer support
            SliverToBoxAdapter(child: Header()),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(left: horizontal, right: horizontal, top: vertical),
                child: buildBody(context, sizingInformation, state),
                // child: Text(sizingInformation.screenSize.width.toString()),
              ),
            ),
            const SliverPadding(padding: EdgeInsets.symmetric(vertical: 100)),
            SliverToBoxAdapter(
              child: Footer(),
            )
          ],
        );
      }),
    );
  }

  ListTile buildNavItem(BuildContext context, WebsitePage page) {
    final theme = Theme.of(context);

    return ListTile(
      title: Text(
        page.getTitle(context),
        style: theme.textTheme.bodyMedium,
      ),
      onTap: () {
        final router = locator<AppRouter>();
        // close drawer
        router.pop();

        // open page
        router.push(page.routeForPage());
      },
    );
  }

  Widget buildBody(BuildContext context, SizingInformation sizingInformation, ResponsiveState state);

  double paddingForState(SizingInformation sizingInformation, ResponsiveState state) {
    final size = sizingInformation.screenSize;

    switch (state) {
      case ResponsiveState.small:
        return 25;
      case ResponsiveState.medium:
        return size.width * 0.15;
      case ResponsiveState.large:
        return size.width * 0.2;
    }
  }

  double verticalPaddingForState(SizingInformation sizingInformation, ResponsiveState state) {
    final size = sizingInformation.screenSize;

    switch (state) {
      case ResponsiveState.small:
        return 10;
      case ResponsiveState.medium:
      case ResponsiveState.large:
        return 100;
    }
  }
}
